# --------------------------------------------------------

# UTILITY FUNCTIONS

# --------------------------------------------------------

#-------------------------------------------------------------------------------
# Analysis report
#-------------------------------------------------------------------------------

#' Reporting correlation analysis report
#'
#' prints results of correlation analysis
#'
#' @param Method A character string giving the name of the correlation analysis (\code{"VIF_COR"}, \code{"VIF_STEP"} or |code{"PW_COR"})
#' @param data A VIF object
#' @param threshold A number giving the correlation threshold
#' @param cor_threshold An optional number giving the correlation threshold for \code{"VIF_COR"} analysis.Default is NULL
#' @export
#' @return A printing message
show_report <- function(Method, data, threshold, cor_threshold=NULL){

	if( Method=="VIF_COR" | Method=="VIF_STEP"){

		if(class(data)!="VIF")
			stop("Argument 'data' must be a 'VIF' object")

		# rename 'method' argument
		if(Method=="VIF_COR")
			method="Variance Inflation Factor pairwise"
		else if(Method=="VIF_STEP")
			method="Variance Inflation Factor stepwise"

		# matrix index of min/max correlation values
		mat_corr = data@corMatrix # make a copy first
		diag(mat_corr) <- NA # put NA on diagonal
		minCoo    = which(abs(mat_corr)==min(abs(mat_corr),na.rm=TRUE),arr.ind=TRUE)
		maxCoo    = which(abs(mat_corr)==max(abs(mat_corr),na.rm=TRUE),arr.ind=TRUE)

		# min/max correlations values
		minCor = mat_corr[minCoo[1],minCoo[2]]
		maxCor = mat_corr[maxCoo[1],maxCoo[2]]

		# Variables with min/max correlations
		var_minCor = rownames(minCoo)
		var_maxCor = rownames(maxCoo)

		# list of variables still correlated
		if(Method=="VIF_STEP")
			List_cor = ShinyBIOMOD::compute_correl(mat_cor=data@corMatrix,thresh=cor_threshold,verbose=FALSE)
		else
			List_cor = ShinyBIOMOD::compute_correl(mat_cor=data@corMatrix,thresh=threshold,verbose=FALSE)

		messages <- sprintf(
		  "<span style='font-size: 22px;'>%s</span><br><br>
  					       <p>
  					       &nbsp;&nbsp;<strong>Method based on:</strong>&nbsp;<span class='label label-primary'>%s</span><br>
  					       &nbsp;&nbsp;<strong>Selected threshold for computations:</strong>&nbsp;%g<br>
                   &nbsp;&nbsp;<strong>Selected variables for analysis:</strong>&nbsp;<i>%s</i><br><br>
                   <hr style='border: 1px solid; background-color: black; width:30%%; text-align:center; margin: 0 auto;'><br>
                   &nbsp;&nbsp;%s%s<br><br>
                   &nbsp;&nbsp;After excluding the collinear variables, the linear correlation coefficients ranges between:<br>
  					       <ul>
  					       <li>%s<br>
  					       <li>%s<br>
  					       </ul>
                   &nbsp;&nbsp;%s<br>
                   </p>
                  ",
		  paste("<span class='label undefined'>Correlation analysis</span>",format(Sys.time(),'%d/%m/%Y %H:%M:%S'),sep=" - "),
		  method,
		  threshold,
		  paste(data@variables, collapse=", "),
      paste0(method," method detected collinearity problems among <span class='label label-danger'>",length(data@excluded),"</span> from the ",length(data@variables)," variables"),
		  if(length(data@excluded)>0L) paste0(": ", paste(data@excluded, collapse=", ")) else "",
		  paste0(round(minCor,4)," ( minimum correlation between <strong>", var_minCor[1],'</strong> and <strong>', var_minCor[2],"</strong> )"),
		  paste0("<span class='label label-success'>",round(maxCor,4),"</span>", '( maximum correlation between <strong>', var_maxCor[1],'</strong> and <strong>',var_maxCor[2],"</strong> )"),
	  	if(length(List_cor)>0L){
	  		paste0("<br><strong>",paste0("<u>Correlation ( >|",cor_threshold,"| ) between variables retained</u>"),"</strong><br><br>",
	  		paste0(sapply(1:length(List_cor), function(i){
	  			paste0('<strong>',names(List_cor)[i],'</strong> is correlated with:<br>',
	  		  paste0('<ul><li>',paste(names(List_cor[[i]]),' (',round(as.vector(List_cor[[i]]),3),')', collapse="</li><li>"),'</li></ul>'))
	  		}), collapse="----<br>")
	  		)
	  	}
	  	else if(Method=="VIF_STEP"){
	  		paste0("<hr style='border-top: 0.5px; text-align: left; background-color: black; width:100%%; margin: 0 auto;'><br>",
	  		"No correlation above threshold |",cor_threshold,"| has been found after excluding collinear variables !!<br>")
	  	}else{
	  	  ""
	  	}
		)

	}
	else if(Method=="PW_COR"){

		# list of variables still correlated
		List_cor = ShinyBIOMOD::compute_correl(mat_cor=data,thresh=threshold,verbose=FALSE)

		messages <- sprintf(
		  "<span style='font-size: 22px;'>%s</span><br><br>
  					       <p>
  					       &nbsp;&nbsp;<strong>Method based on:</strong>&nbsp;<span class='label label-primary'>Pairwise Correlation</span><br>
  					       &nbsp;&nbsp;<strong>Selected threshold for computations:</strong>&nbsp;%g<br>
                   &nbsp;&nbsp;<strong>Selected variables for analysis:</strong>&nbsp;<i>%s</i><br><br>
                   <hr style='border: 1px solid; background-color: black; width:30%%; text-align:center; margin: 0 auto;'><br>
                   &nbsp;&nbsp;%s<br>
                   </p>
                  ",
		  paste("<span class='label undefined'>Correlation analysis</span>",format(Sys.time(),'%d/%m/%Y %H:%M:%S'),sep=" - "),
		  threshold,
		  paste(colnames(data), collapse=", "),
      if(length(List_cor)>0L){
		    paste0("<br><strong>",paste0("<u>Correlation ( >|",threshold,"| ) between variables</u>"),"</strong><br><br>",
		           paste0(sapply(1:length(List_cor), function(i){
		            paste0('<strong>',names(List_cor)[i],'</strong> is correlated with:<br>',
		            paste0('<ul><li>',paste(names(List_cor[[i]]),' (',round(as.vector(List_cor[[i]]),3),')', collapse="</li><li>"),'</li></ul>'))
		           }), collapse="<hr style='border: 0.5px dashed !important; text-align:left !important; background-color: black; width:30%;'/><br>")
		    )
		  }
		  else{
		    paste0("No correlation above threshold |",threshold,"| has been found !!<br>")
		  }
		)

	}

  text.style = "padding-top: 0.5em; padding-bottom:0.5em; padding-left:0.5em;font-family: times, monospace; font-size: 18px;"
  tagList(

    tags$hr(style="border: 2px solid !important; color: #fcc201 !important;"),

    div(style=text.style, HTML(gsub("\\n","", messages[messages !='']))),

    tags$hr(style="border: 2px solid !important; color: #fcc201 !important;")
  )
}

#-------------------------------------------------------------------------------
# Analysis table
#-------------------------------------------------------------------------------

#' Reporting correlation analysis report
#'
#' extracts and prints results of correlation analysis
#'
#' @param mat_cor A correlation matrix or data.frame
#' @param thresh A number giving the correlation threshold
#' @param verbose Logical. Should print the result ? Default is TRUE
#' @export
#' @return A list whose each element "i" is reporting the variables for which the correlation with the ith variable is > threshold desired.
compute_correl <- function(mat_cor,thresh=0.6,verbose=TRUE){

	if(!inherits(mat_cor,c("matrix","data.frame")))
		stop("Argument 'mat_cor' must be a 'matrix' or a 'data.frame'")

	getVariablesCor <- function(x, thresh){
		index = which(abs(x)>thresh)
		cor_values = x[index]
		names(cor_values) <- colnames(mat_cor)[index]
		return(cor_values[order(abs(cor_values),decreasing=T)])
	}

	# Assign NA to upper and diagonal values of the matrix  (to avoid redundant information and correlation of variable with itself)
	mat_cor[upper.tri(mat_cor,diag=TRUE)]<- NA

	# Find correlations
	List_var_correl <- apply(mat_cor,2,getVariablesCor,thresh=thresh)

	# Remove null correlations
	List_var_correl <- List_var_correl[lengths(List_var_correl)>0L]

	if(verbose){
		cat('*************************************************************************************************\n\n')
		cat(' Correlation analysis based on Pairwise Correlation method and according to a threshold of',thresh,'\n')
		cat(' was performed on a dataset with the following variables : \n\n')
		cat(' ',colnames(mat_cor),'\n\n')
		cat('*************************************************************************************************\n\n')
		if(length(List_var_correl)>0){
			cat(sprintf("%s\n* Correlation list *\n%s\n\n",paste(rep("*",nchar("Correlation list")+2*2),collapse=""),paste(rep("*",nchar("Correlation list")+2*2),collapse="")))

			for(i in 1:length(List_var_correl)){
				cat('Variable',names(List_var_correl)[i],'is correlated with: \n\n')
				cat('',sprintf('%s (%g)\n',names(List_var_correl[[i]]), as.vector(List_var_correl[[i]])))
				cat('-------------------------------------------------------------------------------------------------\n\n')
			}
		}
		else{
			cat('No correlations above threshold',thresh,'has been found !!\n\n')
			cat('-------------------------------------------------------------------------------------------------\n\n')
		}
	}

	return(List_var_correl)
}


