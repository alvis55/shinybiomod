#' From a list to a command-line for MaxEnt
#'
#' Reads a named list and returns an executing command line for MaxEnt
#'
#' @param argList A list object whose elements refer to Maxent arguments
#'
#' @export
List2MaxEntCommand <- function(argList){

  if(missing(argList))
    stop("Argument argList is missing")

  if(sum(lengths(argList))==0L)
    stop("Argument argList is of length 0L")

  if(all(sapply(names(argList),nchar,USE.NAMES=F)==0L))
    stop("All elements in argList must be named")

  cmd = ''
  arg_names  = names(argList)

  for(arg in arg_names){

    flag = NULL

    flag = switch(arg,

                  'responsecurves' 	=	if(argList[[arg]])'-P' else '',
                  'pictures' 			=	paste0(arg,'=',argList[[arg]]),
                  'jackknife' 		=	if(argList[[arg]]) '-J' else '',
                  'outputformat' 		=	paste0(arg,'=',argList[[arg]]),
                  'outputfiletype' 	=	paste0(arg,'=',argList[[arg]]),
                  #outputdirectory' 	=	paste('-o',argList[[arg]]),
                  'projectionlayers' 	=	paste('-j',argList[[arg]]),


                  #samplesfile' 		=	paste('-s',argList[[arg]])
                  #environmentallayers'=	paste('-e',argList[[arg]])

                  'randomseed'	 	=	paste0(arg,'=',argList[[arg]]),

                  'logscale' 			=	paste0(arg,'=',argList[[arg]]),
                  'warnings' 			=	paste0(arg,'=',argList[[arg]]),

                  'tooltips' 			=	paste0(arg,'=',argList[[arg]]),
                  #askoverwrite'		= 	if(argList[[arg]]) '-r' else '',

                  'skipifexists'		= 	if(argList[[arg]]) '-S' else '',

                  'removeduplicates'	=	paste0(arg,'=',argList[[arg]]),


                  'writeclampgrid'	= 	paste0(arg,'=',argList[[arg]]),

                  'writemess'			= 	paste0(arg,'=',argList[[arg]]),

                  'randomtestpoints'	=	paste('-X',argList[[arg]]),
                  'betamultiplier' 	=	paste('-b',argList[[arg]]),
                  'maximumbackground'	=	paste('-B',argList[[arg]]),
                  'biasfile'			=	paste0(arg,'=',argList[[arg]]),
                  'biastype'      = paste0(arg,'=',argList[[arg]]),


                  'testsamplesfile'	= 	paste('-T',argList[[arg]]),


                  'replicates'		=	paste0(arg,'=',argList[[arg]]),
                  'replicatetype'		=	paste0(arg,'=',argList[[arg]]),



                  'perspeciesresults'	=	paste0(arg,'=',argList[[arg]]),
                  'writebackgroundpredictions'=	paste0(arg,'=',argList[[arg]]),
                  'responsecurvesexponent'=	paste0(arg,'=',argList[[arg]]),
                  'linear'			=	if(!argList[[arg]])'-l' else '',
                  'quadratic'			= 	if(!argList[[arg]])'-q' else '',
                  'product' 			=	if(!argList[[arg]])'-p' else '',
                  'threshold'			=	if(!argList[[arg]]) 'nothreshold' else '',
                  'hinge'				= 	if(!argList[[arg]]) 'nohinge' else '',
                  'addsamplestobackground'=	if(!argList[[arg]])'-d' else '',
                  'addallsamplestobackground'= paste0(arg,'=',argList[[arg]]),
                  'autorun'			= 	if(argList[[arg]])'-a' else '',
                  'writeplotdata'		=	paste0(arg,'=',argList[[arg]]),
                  'fadebyclamping'	= 	paste0(arg,'=',argList[[arg]]),

                  'extrapolate'		=	paste0(arg,'=',argList[[arg]]),
                  'visible'			=	if(!argList[[arg]]) '-z' else '',
                  'autofeature'		= 	if(!argList[[arg]]) '-A' else '',
                  'doclamp'			= 	paste0(arg,'=',argList[[arg]]),
                  'outputgrids'		= 	if(!argList[[arg]]) '-x' else '',
                  'plots'				= 	if(!argList[[arg]]) 'noplots' else '',
                  'appendtoresultsfile'= 	paste0(arg,'=',argList[[arg]]),
                  'maximumiterations'=	paste('-m',argList[[arg]]),
                  'convergencethreshold'= paste('-c',argList[[arg]]),
                  'adjustsampleradius'= 	paste0(arg,'=',argList[[arg]]),

                  'threads'			= paste0(arg,'=',argList[[arg]]),
                  'lq2lqptthreshold'	= paste0(arg,'=',argList[[arg]]),
                  'l2lqthreshold'		= paste0(arg,'=',argList[[arg]]),
                  'hingethreshold'	= paste0(arg,'=',argList[[arg]]),
                  'beta_threshold'	= paste0(arg,'=',argList[[arg]]),
                  'beta_categorical'	= paste0(arg,'=',argList[[arg]]),
                  'beta_lqp'			= paste0(arg,'=',argList[[arg]]),
                  'beta_hinge'		= paste0(arg,'=',argList[[arg]]),
                  'logfile'			= paste0(arg,'=',argList[[arg]]),
                  'cache'				= paste0(arg,'=',argList[[arg]]),
                  'defaultprevalence'	= paste0(arg,'=',argList[[arg]]),

                  'applythresholdrule'= shQuote(paste0(arg,'=',argList[[arg]]),type='sh'),
                  'togglelayertype'	=	paste('-t',argList[[arg]]),
                  'togglespeciesselected'= paste('-E',argList[[arg]]),
                  'togglelayerselected'= 	paste('-N',argList[[arg]]),
                  'verbose'			= 	if(argList[[arg]])	'-v' else '',
                  'allowpartialdata' = paste0(arg,'=',argList[[arg]]),
                  'prefixes'			= paste0(arg,'=',argList[[arg]]),
                  'nodata'			=	paste('-n',argList[[arg]])
    )
    if(is.null(flag))
      stop(paste('Argument',arg,'does not exist'))

    if(arg=='visible'){
      if(!argList[[arg]])
        flag <- paste(flag,'-a')
    }
    cmd = paste(cmd,flag)
  }

  return(cmd)
}
