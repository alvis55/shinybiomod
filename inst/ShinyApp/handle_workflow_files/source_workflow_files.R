source("handle_workflow_files/handle_workflow.R",local=TRUE)
source("modules/dmap_report_module.R")
source("server_app_files/raster_layers.R",local=TRUE)
source("server_app_files/ArcGIS_Settings.R",local=TRUE)
source("server_app_files/regionMap.R",local=TRUE)
source("server_app_files/input_data_user.R",local=TRUE)
source("server_app_files/mapDisplay.R",local=TRUE)
source("server_app_files/Background_extent_methods.R",local=TRUE)
source("server_app_files/correlation_functions.R",local=TRUE)
source("server_app_files/PCA_methods.R",local=TRUE)
source("server_app_files/Sparse_PCA_methods.R",local=TRUE)
source("server_app_files/Best_Subsets_Model_Selection.R",local=TRUE)
source("server_app_files/spThinning.R",local=TRUE)
source("server_app_files/prepare_data.R",local=TRUE)
source("server_app_files/data_worldclim.R",local=TRUE)
source("server_app_files/BiomodMaxentSettings.R",local=TRUE)
source("server_app_files/BiomodModelOptions.R",local=TRUE)
source("server_app_files/BiomodFormating.R",local=TRUE)
source("server_app_files/BiomodModeling.R",local=TRUE)
source("server_app_files/BiomodEnsModeling.R",local=TRUE)
source("server_app_files/BiomodEnsForecasting.R",local=TRUE)
source("server_app_files/BiomodPerfMetric.R",local=TRUE)
source("server_app_files/BiomodRespFunc.R",local=TRUE)
source("server_app_files/projRaster.R",local=TRUE)
source("server_app_files/projMap.R",local=TRUE)
source("server_app_files/BiomodThresholdingMethods.R",local=TRUE)
source("server_app_files/ArcGIS_Mapping.R",local=TRUE)

# delay reactive expression of slider range of environmental maps to plot
slider_range_map_layers_user_env_data <- shiny::debounce(reactive(input$user_range_of_map_layers), millis=1000)
# --------------------------------------------------------

# OBSERVERS

# --------------------------------------------------------

# enable to search the raster layer folder recursively
observeEvent(input$use_recursive_search, ignoreNULL=TRUE, {
  updateCheckboxInput(session, 'use_recursive_search', value=input$use_recursive_search)
})

# enable load button only if the directory contains raster layers that have the same extent and the same resolution
observe({

	test = FALSE
	if(!is.null(input$rasters_layers_directory)){
		# Regular expression to check for rasters layer with different formats
		rast_formats 	<- "(*.grd$)|(*.asc$)|(*.bil$)|(*.sdat$)|(*.rst$)|(*.tif$)|(*.envi$)|(*.img$)|(*hdr.adf$)"
		rast_dir 		<- readDirectoryInput(session, 'rasters_layers_directory')

		# conditions
		test 			<- !is.null(rast_dir) && length(list.files(rast_dir, pattern = rast_formats, full.names = TRUE, recursive=input$use_recursive_search))>0L

	}

	if(!test){
		shinyjs::show("user_raster_dir_warning")
	}
	else{
	  shinyjs::hide("user_raster_dir_warning")

	  list_rasters <- lapply(list.files(rast_dir, pattern = rast_formats, full.names = TRUE, recursive=input$use_recursive_search), raster::raster)

	  # check raster extent
	  test <- do.call(raster::compareRaster, args=c(list_rasters, extent=TRUE, crs=FALSE, res=FALSE, stopiffalse = FALSE), quote=TRUE)

	  if(!test)
	    shinyjs::show("user_raster_extent_warning")
	  else
	    shinyjs::hide("user_raster_extent_warning")

	  # check raster resolution
	  test <- do.call(raster::compareRaster, args=c(list_rasters, extent=FALSE, crs=FALSE, res=TRUE, stopiffalse = FALSE), quote=TRUE)

	  if(!test)
	    shinyjs::show("user_raster_resolution_warning")
	  else
	    shinyjs::hide("user_raster_resolution_warning")
	}

  toggleState("load_user_layers",
		condition = test
	)

})

# disable directory containing raster layers
observeEvent(input$resetRastersInput,ignoreNULL=TRUE,{

  if(input$resetRastersInput > 0){
    # set directory to NULL
    updateDirectoryInput(session, 'rasters_layers_directory', value = '#')
    # reset the path the environment variable
    Sys.unsetenv("SHINY_BIOMOD_RASTER_DIR")
    # remove temporary files
    if(length(list.files(dirname(rasterTmpFile()),'^myRaster',full.names=TRUE))>0L)
      file.remove(list.files(dirname(rasterTmpFile()),'^myRaster',full.names=TRUE))
    hasLoadedEnvData(FALSE)
  }

})

# updates raster layer directory
observeEvent(
	ignoreNULL = TRUE,

	eventExpr = {
		input$rasters_layers_directory
	},
	handlerExpr = {
		# condition prevents handler execution on initial app launch
		if (input$rasters_layers_directory > 0 ) {
      # launch the directory selection dialog with initial path read from the widget
      path = choose.dir(default = readDirectoryInput(session, 'rasters_layers_directory'))
      path <- gsub("\\\\","/",path)
      rasters_dir <<- path
      # save the path in an environment variable
      Sys.setenv(SHINY_BIOMOD_RASTER_DIR=path)
      # update the widget value
      updateDirectoryInput(session, 'rasters_layers_directory', value = path)
		}
	}
)


# updates environmental dataset
observeEvent(

	eventExpr={
		input$load_user_layers
	},

	handlerExpr = {
		withBusyIndicatorServer(buttonId="load_user_layers",

			expr = {

				Sys.sleep(1)
				if(!is.null(rasterLayers())){
					updateSelectInput(session,"niche_layers_from",selected="user_layers")
				  hasLoadedEnvData(TRUE)
					rasterLayers()
					rasters$origins <- unique(c(rasters$origins, "Your own raster layers" = "user_layers"))
				}
				else
					stop("Loading of rasters failed")
			}
		)
	}
)

# hide the tip info div
observeEvent(input$tip_user_layer_got_it, ignoreNULL=TRUE, {
  if(input$tip_user_layer_got_it>0)
    shinyjs::hide("user_raster_layer_info")
})

# open the .Rprofile file if available when clicking on the link
observeEvent(input$edit_R_profile, ignoreNULL=TRUE, {
  if(nzchar(Sys.getenv("R_PROFILE")) && input$edit_R_profile>0)
    try(file.edit(Sys.getenv("R_PROFILE")),silent=TRUE)
})

# show/hide user map display options
observeEvent(list(input$load_user_layers,rasterLayers()), ignoreNULL=TRUE,{
	if(!is.null(input$load_user_layers) && input$load_user_layers>0 && nlayers(rasterLayers())>1){
		shinyjs::show('user_map_display_options')
	}
	else
		shinyjs::hide('user_map_display_options')
})

# Observer to update input$user_range_of_map_layers
observe({
	# Get maximun number of layers
	mx = if(!is.null(rasterLayers())) nlayers(rasterLayers()) else 1
	# Update slider input
	updateSliderInput(session, 'user_range_of_map_layers', value = slider_range_map_layers_user_env_data(), max=mx)
})


# Observer to update input$palette_name
observe({
  pals= NULL
  # Get available palette from palette type
  if(!is.null(input$user_map_palette_type))
    pals = row.names(subset(brewer.pal.info,category==input$user_map_palette_type))

  # Update select input for palette name
  updateSelectizeInput(session, 'user_map_palette_name', choices = pals)

})

# Observer to update input$ncolors
observe({
  # Get maximun number of colors from palette name
  maxColors = brewer.pal.info[input$user_map_palette_name,"maxcolors"]

  # Update select input for palette name
  updateNumericInput(session, 'user_map_ncolors', max=maxColors)
})

# Observer to update input$colorRamp_checkbox
observeEvent(input$user_map_ncolors,{
  updateNumericInput(session, "user_map_ncolors", value = input$user_map_ncolors)
})

# Observer to update input$colorRamp_checkbox
observeEvent(input$user_map_colorRamp_checkbox,{
  updateCheckboxInput(session, "user_map_colorRamp_checkbox", value = input$user_map_colorRamp_checkbox)
})



# restarts the session when the user click on the refresh button
# observeEvent(input$refreshSession, {
#
# 	# reset working directory if necessary
# 	if(exists('oldwd')) setwd(oldwd)
#
# 	# remove r objects
# 	robjs <- ls()
# 	if(!is.null(robjs)) rm(robjs)
#
# 	# remove temporary files
# 	fn <- paste0(dirname(rasterTmpFile()),'/myPolygon')
# 	if(file.exists(fn)) file.remove(list.files('^myPolygon',dirname(fn),full.names=TRUE))
#
# 	session$reload()
# })

# updates the workflow directory
observeEvent(
	ignoreNULL = TRUE,

	eventExpr = {
		input$workflow_directory
	},
	handlerExpr = {
		# condition prevents handler execution on initial app launch
		if (input$workflow_directory > 0) {
			  # launch the directory selection dialog with initial path read from the widget
			  path = choose.dir(default = readDirectoryInput(session, 'workflow_directory'))
			  path <- gsub("\\\\","/",path)
			  # save the path in an environment variable
			  workflow_output_dir <<- path
			  if(Sys.getenv("SHINY_BIOMOD_OUTPUT_DIR")!=path) Sys.setenv(SHINY_BIOMOD_OUTPUT_DIR=path)
			  # update the widget value
			  updateDirectoryInput(session, 'workflow_directory', value = path)
		}
	}
)


# hides Tip info from the main panel of the General settings tab
observeEvent(input$tip_working_dir_got_it, ignoreNULL=TRUE,{
  if(input$tip_working_dir_got_it>0)
    shinyjs::hide("working_dir_info")
})

# open the .Rprofile file if available when clicking on the link
observeEvent(input$edit_R_profile_, ignoreNULL=TRUE, {
  if(nzchar(Sys.getenv("R_PROFILE")) && input$edit_R_profile_>0)
    try(file.edit(Sys.getenv("R_PROFILE")),silent=TRUE)
})

# updates (locates) the tab panel when the user navigate
observeEvent(input$navb,{
	updateTabsetPanel(session, inputId='navb', selected=input$navb)
})
# reminder of navigation in Biomod panel
reminder_navbiomod <- reactiveValues(nav_biomodmetric=NULL, nav_biomodrespfunc=NULL, nav_biomodformating=NULL, nav_biomodmodeling=NULL, nav_biomodensmodeling=NULL, nav_biomodensforecasting=NULL, nav_biomodthresholding=NULL)


# --------------------------------------------------------

# REACTIVE FUNCTIONS

# --------------------------------------------------------

# reads and returns raster layers directory
rasterLayersDir <- reactive({

  input$rasters_layers_directory
	path <- readDirectoryInput(session, 'rasters_layers_directory')

	if(length(path)>0L && nchar(path)>0L)
		return(path)
	else
		return(NULL)
})

# returns stack of environmental variables
rasterLayers <- reactive({

	layers_dir <-rasterLayersDir()

	isolate({
		if( !is.null(layers_dir) && nchar(layers_dir)>0L ){
		  r <- ShinyBIOMOD::rlayers_shbmd(layers_dir)
		  rplcmt <- stringr::str_extract(names(r),"\\.[1-9]+$")
		  if(any(!is.na(rplcmt)))
		    names(r) <- stringr::str_replace(names(r),"\\.[1-9]+$",gsub("\\.","",paste0("_shbmd",rplcmt)))
		  return(r)
		}
		return(NULL)
	})
})


# reads and returns workflow directory
workflowDir <- reactive({

	path <- readDirectoryInput(session, 'workflow_directory')
	if(is.null(path))
		path <- getwd()

	if(length(path)>0L)
		return(gsub("\\/$","",path)) # remove trailing slash
	else
		return(NULL)
})

user_map_Palette <- reactive({

  if(is.null(input$user_map_palette_name) || nchar(input$user_map_palette_name)<1)
    return(NULL)

  if(!is.null(input$user_map_colorRamp_checkbox) && input$user_map_colorRamp_checkbox==TRUE)
    return( suppressWarnings(colorRampPalette(brewer.pal(n=input$user_map_ncolors, name=input$user_map_palette_name))(input$user_map_ncolors_ColorRamp)) )

  if(!is.null(input$user_map_colorRamp_checkbox) && input$user_map_colorRamp_checkbox==FALSE)
    return( suppressWarnings(brewer.pal(n=input$user_map_ncolors, name=input$user_map_palette_name)) )

})

# returns range of worldclim raster layers
user_range_layers <- eventReactive(slider_range_map_layers_user_env_data(), ignoreNULL=TRUE, {

	from = input$user_range_of_map_layers[1]
	to = input$user_range_of_map_layers[2]

	return(seq(from,to))
})

# --------------------------------------------------------

# OUTPUTS

# --------------------------------------------------------

# displays user input explanatory variables raster layers
output$user_explvar_layers <- renderPlot({

    rLayers <- if(hasUploadedRegionMap()) myRegionRasters() else rasterLayers()
    if(is.null(rLayers) && !is.null(raster_processing_output_signal$region_cropped)
       && (!raster_processing_output_signal$region_cropped || !raster_processing_output_signal$region_masked) ){
      message <- "Cropping/masking failed !\nchange region extent"
      x <- -10:10
      y <- x
      plot(x,y,type="n", xlab="", ylab="",cex=2)
      text(0,0,message,cex=4,col="grey")
    }
  	else if(!is.null(input$load_user_layers) && input$load_user_layers > 0 && !is.null(rLayers)){
		if(!is.null(user_map_Palette()))
			plot(rLayers[[user_range_layers()]],col=user_map_Palette())
		else
			plot(rLayers[[user_range_layers()]])
	}

},width=800, height=600)


# --------------------------------------------------------


# action(s) to complete when session is ending
onStop(function(){

	cat("\n\n...[Closing ShinyBIOMOD]...")

	# remove temporary files
	if(length(list.files(dirname(rasterTmpFile()),'^my(Polygon|Region|Rasters|user|wc|Data)|^biomodEF',full.names=TRUE))>0L)
	  file.remove(list.files(dirname(rasterTmpFile()),'^my(Polygon|Region|Rasters|user|wc|Data)|^biomodEF',full.names=TRUE))
  if(length(list.files(dirname(rasterTmpFile()),'\\.csv$',full.names=TRUE))>0L)
    file.remove(list.files(dirname(rasterTmpFile()),'\\.csv$',full.names=TRUE))
  if(length(list.files(dirname(rasterTmpFile()),'*_dat_pca.txt$',full.names=TRUE))>0L)
    file.remove(list.files(dirname(rasterTmpFile()),'*_dat_pca.txt$',full.names=TRUE))
  if(length(list.files(dirname(rasterTmpFile()),'*_dat_spca_predictor.txt$',full.names=TRUE))>0L)
    file.remove(list.files(dirname(rasterTmpFile()),'*_dat_spca_predictor.txt$',full.names=TRUE))

  # restore Renviron & Rprofile
  if(!identical(path2Renviron(), character(0)) && nzchar(path2Renviron()) && file.exists(path2Renviron()))
    readRenviron(path2Renviron())

  if(!identical(path2Rprofile(), character(0)) && nzchar(path2Rprofile()) && file.exists(path2Rprofile()))
    source(path2Rprofile())

	cat("DONE\n")
})

# --------------------------------------------------------
