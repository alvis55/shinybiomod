# Name: exportPNGMap.py
# Description: Export map document as PNG 
# Requirements: ArcGIS Mapping Extension

# Import system modules
import os
import re
import arcpy
from arcpy import env
from arcpy.mapping import *

# Get command line arguments
import sys
args = sys.argv

# Define current working space 
env.workspace = args[1]

# Allow overwriting
env.overwriteOutput = True

# Set local variables
template	= args[2]
outmap		= args[3]

# Check out ArcGIS Mapping extension license
arcpy.CheckOutExtension('Mapping')

# read the original template 
mxd = MapDocument(template)

# output map
#outmap = os.path.join(os.path.dirname(outdir),re.sub('_MapTemplate.mxd','.png',os.path.basename(template))).replace("\\","/")

# export
ExportToPNG(mxd, outmap, resolution=300)
