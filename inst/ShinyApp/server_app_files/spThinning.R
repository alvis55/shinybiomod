
spatial_thinning_analysis_selected_dataset <- reactiveValues()
spatial_thinning_analysis_output_message <- ""

# --------------------------------------------------------

# TRIGGERS

# --------------------------------------------------------
isThinned <- reactiveVal(FALSE)
isPrinted <- reactiveVal(FALSE)

can_save_thinned_data <- reactiveVal(FALSE)
can_delete_thinned_data <- reactiveVal(FALSE)

#---------------------------------------------------------

# OBSERVERS

# --------------------------------------------------------
observe({

  cond1 <- !is.null(input$spthin_NND) && !is.na(input$spthin_NND) && input$spthin_NND >0L
  cond2 <- !is.null(input$spthin_Nreps) && !is.na(input$spthin_Nreps) && input$spthin_Nreps>0L

  toggleState(id="spthin_thin", condition=cond1 && cond2)

})

observe({

  toggleState(id="save_spThin_data", condition=can_save_thinned_data())

})

observe({

  toggleState(id="delete_spThin_data", condition=can_delete_thinned_data())

})

# shows/hides save thinned occurrence data option
observeEvent(input$spthin_thin, ignoreNULL=TRUE,{

  	if(input$spthin_thin>0 && isThinned()){

		shinyjs::show('save_button_spThin_data')
	}

})


# shows/hides species selector for thinning analysis
observeEvent(input$spThin_data_from_occ_dataset, ignoreNULL=TRUE,{
	if(nchar(input$spThin_data_from_occ_dataset)>0L){
		shinyjs::show('spThin_data_with_selected_species')
	}
	else{
		shinyjs::hide('spThin_data_with_selected_species')
	}
})

# updates the selection of the thinned dataset
observe({
  if(!is.null(input$spthin_data_thinned) && !is.null(input$spthin_Nreps))
	  updateNumericInput(session, 'spthin_data_thinned',	value=min(input$spthin_data_thinned, input$spthin_Nreps), max = input$spthin_Nreps)
})

# save thinned occurrence points shape file
observeEvent(

	ignoreNULL = TRUE,

	eventExpr={
		input$save_spThin_data
	},

	handlerExpr = {

		withBusyIndicatorServer(buttonId="save_spThin_data",

			expr = {

			  Sys.sleep(1)

				if(isThinned()){

					# retrieve ID's from original point data
					#init_pts 				<- na.omit(data_spthin()[,colnames(data_spthin())%in%c('SPEC','LONG','LAT','RESP')])
					#thinned_occ_dat 	<- data_thinned[[ifelse(is.null(input$spthin_data_thinned),1,input$spthin_data_thinned)]]
					#id_thinned_pts 	<- sapply(apply(thinned_occ_dat,1,paste,collapse='_'), match, table=apply(init_pts[,c('LONG','LAT')],1,paste,collapse='_'))
				  spthin_data_thinned <- min(input$spthin_data_thinned,input$spthin_Nreps)

				  # define points
				  thinned_occ_dat		<- read.csv(file.path(dirname(rasterTmpFile()), paste0(input$spThin_data_species,"_",input$spthin_NND,"_thin",if(is.null(spthin_data_thinned)) 1 else spthin_data_thinned,".csv"))) #data_thinned[[ifelse(is.null(spthin_data_thinned),1,spthin_data_thinned)]]

					#names(id_thinned_pts)	<- rep(input$spthin_NND, length(id_thinned_pts))
					spatial_thinning_analysis_selected_dataset[[input$spThin_data_species]] <- thinned_occ_dat
					can_save_thinned_data(FALSE)
					can_delete_thinned_data(TRUE)

				}
			}
		)
	}
)

# delete background extent shape file
observeEvent(

  ignoreNULL = TRUE,

  eventExpr={
    input$delete_spThin_data
  },

  handlerExpr = {

    withBusyIndicatorServer(buttonId="delete_spThin_data",

        expr = {

          Sys.sleep(1)

          if (!is.null(reactiveValuesToList(spatial_thinning_analysis_selected_dataset)[[input$spThin_data_species]])){
            spatial_thinning_analysis_selected_dataset[[input$spThin_data_species]] <- NULL
            can_delete_thinned_data(FALSE)
            can_save_thinned_data(TRUE)
          }
        }
    )
  }
)


# runs the thinning process
# TODO: use another library for Ripley's function (spatstat instead of splancs)? kinhom in spatstat ?
observeEvent(

  ignoreNULL=TRUE,

  eventExpr={
    input$spthin_thin
  },

  handlerExpr = {

		withBusyIndicatorServer(buttonId="spthin_thin",

			expr = {

  			    if(input$spthin_thin>0){

  			    dat_spthin = data_spthin()
  			    previous.files <- list.files(dirname(rasterTmpFile()), pattern=paste0(paste0(input$spThin_data_species,"_",input$spthin_NND),"_thin[[:digit:]]{1,}\\.csv$"),full.names=TRUE)

            # remove already computed datasets
  			    if(length(previous.files)>0L){
  			      file.remove(previous.files)
  			    }

  			    if(!is.null(dat_spthin)){

    			      ArgList <- list(

    			        loc.data = na.omit(dat_spthin[,colnames(dat_spthin)%in%c('SPEC','LONG','LAT','RESP')]),

    			        thin.par = input$spthin_NND,  reps = input$spthin_Nreps,

    			        locs.thinned.list.return = TRUE,

    			        write.files=FALSE,

    			        #max.files = input$spthin_Nreps,

    			        #out.dir = dirname(rasterTmpFile()),  out.base = paste0(input$spThin_data_species,"_",input$spthin_NND),

    			        write.log.file =FALSE,

    			        verbose = FALSE
    			      )
    			      ArgList = ArgList[lengths(ArgList)>0L]

    			      # process the thinning
    			      started.at = Sys.time()
    			      suppressWarnings(locs.thinned.list <- do.call(spThin::thin, ArgList))
    			      finished.at = Sys.time()

    			      # write files on the disk
    			      for (k in  1:length(locs.thinned.list))
    			        write.csv(setNames(as.data.frame(locs.thinned.list[[k]]), c('LONG','LAT')), row.names=FALSE, file=file.path(dirname(rasterTmpFile()), paste0(input$spThin_data_species,"_",input$spthin_NND,"_thin",k,".csv")))

    			      # regex: "\\(([^()]+)\\)"
    			      # matches anything between the parentheses (+ the parentheses)
  			        list_data_thinned <- list.files(dirname(rasterTmpFile()), pattern=paste0(paste0(gsub("\\(([^()]+)\\)","(.*?)",input$spThin_data_species),"_",input$spthin_NND),"_thin[[:digit:]]{1,}\\.csv$"),full.names=TRUE)

  			        if(length(list_data_thinned)>0L){

  			        isThinned(TRUE)

      				  spatial_thinning_analysis_running_time  <- finished.at - started.at # calculate amount of time elapsed
      				  spatial_thinning_analysis_timestamp     <- format(Sys.time(),'%d/%m/%Y %H:%M:%S')

      				  spthin_data_thinned <- min(input$spthin_data_thinned,input$spthin_Nreps)
      					# define points
      					init_pts 				<- dat_spthin[,colnames(dat_spthin)%in%c('SPEC','LONG','LAT','RESP')]
      					thinned_pts 		<- locs.thinned.list[[ifelse(is.null(spthin_data_thinned),1,spthin_data_thinned)]]#read.csv(grep(paste0(input$spThin_data_species,"_",input$spthin_NND,"_thin",if(is.null(spthin_data_thinned)) 1 else spthin_data_thinned,"\\.csv$"), list_data_thinned,value=TRUE)) #data_thinned[[ifelse(is.null(spthin_data_thinned),1,spthin_data_thinned)]]
      					thinned_pts     <- setNames(as.data.frame(thinned_pts), c('LONG','LAT'))

      					#converted_thinned_pts 	<- t(mapply(LLToUTM,thinned_pts[,'LONG'],thinned_pts[,'LAT']))
      					id_thinned_pts 	<- which(duplicated(rbind(thinned_pts,unique(init_pts[,c('LONG','LAT')]))))-nrow(thinned_pts) #sapply(apply(thinned_pts[,-1],1,paste,collapse='_'), match, table=apply(init_pts[,c('LONG','LAT')],1,paste,collapse='_'))

      					# print summary text
      					output$spthin_summary_text_output <- shiny::renderUI({

      					  data_thinned <- lapply(list_data_thinned, read.csv)

      					  spatial_thinning_analysis_output_message <<- sprintf(
      					                    "<span style='font-size: 22px;'>%s</span><br><br>
      					                     <p>
      					                     <strong>Species:</strong>&nbsp;<i>%s</i><br>
      					                     <strong>Spatial thinning distance:&nbsp;</strong><span class='label label-primary'>%g km</span><br>
      					                     <strong>Number of repetitions of the spatial thinning:&nbsp;</strong> %g<br>
      					                     <strong>Maximum number of occurrence records retained:</strong>&nbsp;%g
      					                     <ul>
      					                     <li><strong>%g</strong> dataset(s) retained the maximum number of records.</li>
      					                     </ul>
      					                     <strong>Number of occurrence records retained in the current dataset:</strong>&nbsp; <span class='label label-success'>%g</span><br>
      					                     <ul>
      					                     <li><strong>%g</strong> occurrence records initially.</li>
      					                     <li><span class='label label-danger'><strong>%g</strong></span> records were deleted.</li>
                                     </ul>
      					                     <strong>Running time:</strong>&nbsp; %g %s.
      					                     </p>",
      					                    paste(isolate(input$spThin_data_species), spatial_thinning_analysis_timestamp,sep=" - "),
      					                    gsub("_|\\."," ",isolate(input$spThin_data_species)),
      					                    isolate(input$spthin_NND),
      					                    isolate(input$spthin_Nreps),
      					                    max(sapply(data_thinned,nrow))[1],
      					                    sum(sapply(data_thinned,nrow)==max(sapply(data_thinned,nrow))[1]),
      					                    nrow(thinned_pts),
      					                    nrow(init_pts),
      					                    nrow(init_pts)-nrow(thinned_pts),
      					                    as.numeric(spatial_thinning_analysis_running_time), attr(spatial_thinning_analysis_running_time,'units')
      					  )
      					  # real palette
      					  #f5e6cc
      					  # interesting color
      					  #7FC97F
      					  tagList(

        					  tags$hr(style="border: 2px solid !important; color: #fcc201 !important;"),

              			#div(
              					fluidRow(style='border-radius: ; background-color: ; font-family: times, monospace; font-size: 18px;',
              					  column(10,
              					       column(9,
              					          div(id="spThin_output_message", HTML(gsub("\\n","",spatial_thinning_analysis_output_message[spatial_thinning_analysis_output_message!=''])))
              					        ),
            					         column(2,
            					          br(),
            					          numericInput(inputId="spthin_data_thinned", label="change dataset", value=1, min=1),
            					          withBusyIndicatorUI(bsButton(inputId="save_spThin_data", style="success", icon=icon('inbox', lib='font-awesome'), label='save')),
            					          withBusyIndicatorUI(bsButton(inputId="delete_spThin_data", style="danger", icon=icon('trash-alt', lib='font-awesome'), label='delete', disabled=TRUE))
            					         )
      					          )
              		    ),
        					  tags$hr(style="border: 2px solid !important; color: #fcc201 !important;")
      					    #)
      					  )
      					})
      					isPrinted(TRUE)
      					spthin_selected_data_thinned <- which.max(sapply(locs.thinned.list, nrow))[1]
      					updateNumericInput(session,'spthin_data_thinned', value = spthin_selected_data_thinned, max=input$spthin_Nreps)
      					shinyjs::show('save_button_spThin_data')
  			        }else{
  			          notifText <-"<span style='font-size: 18px;'><br><i, class='fa fa-exclamation-triangle'></i> Unable to find the filtered dataset(s) !</span>"
  			          shiny::showNotification(HTML(notifText), type="warning", duration=5)
  			        }
            }
          }
			  }
		)
})


observeEvent(input$spthin_data_thinned, ignoreNULL=TRUE, {

    if(isPrinted()){

      # define points
      dat_spthin = data_spthin()
      init_pts 				<- dat_spthin[,colnames(dat_spthin)%in%c('SPEC','LONG','LAT','RESP')]
      spthin_data_thinned <- min(input$spthin_data_thinned,input$spthin_Nreps)
      thinned_pts 		<- read.csv(file.path(dirname(rasterTmpFile()), paste0(input$spThin_data_species,"_",input$spthin_NND,"_thin",if(is.null(spthin_data_thinned)) 1 else spthin_data_thinned,".csv")))

      pattern1 = "\\<span class\\='label label\\-success'\\>[0-9]+\\<\\/span\\>\\<br\\>"
      replacmt1 = sprintf("\\<span class\\='label label\\-success'\\>%g\\<\\/span\\>\\<br\\>",nrow(thinned_pts))
      spatial_thinning_analysis_output_message <<- gsub(pattern1, replacmt1, spatial_thinning_analysis_output_message,perl=TRUE)

      pattern2 = "\\<span class\\='label label\\-danger'\\>\\<strong\\>[0-9]+\\<\\/strong\\>\\<\\/span\\> records were deleted\\."
      replacmt2 = sprintf("\\<span class\\='label label\\-danger'\\>\\<strong\\>%g\\<\\/strong\\>\\<\\/span\\> records were deleted\\.",nrow(init_pts)-nrow(thinned_pts))
      spatial_thinning_analysis_output_message <<- gsub(pattern2, replacmt2, spatial_thinning_analysis_output_message,perl=TRUE)

      shinyjs::html(id="spThin_output_message", html=HTML(gsub("\\n","",spatial_thinning_analysis_output_message[spatial_thinning_analysis_output_message!=''])))

    }
})


# updates occurrence datasets available
observe({
	spThin_data_from_occ_dataset <- NULL

	# if(!is.null(myBiomodFormatedObj())){
		# spThin_data_from_occ_dataset <- c(spThin_data_from_occ_dataset, "Your Biomod Presence-Only dataset" = "biomod_po_formated")
		# AnyPA <- any(do.call('c',lapply(myBiomodFormatedObj(), function(x) if(class(x) %in% c("BIOMOD.formated.data","BIOMOD.formated.data.PA")) any(x@data.species[!is.na(x@data.species)]==0) else NULL))) # look into each biomod formated object found if response variable has 0's i.e. true absences
		# if(AnyPA){
			# spThin_data_from_occ_dataset = spThin_data_from_occ_dataset[spThin_data_from_occ_dataset!="biomod_po_formated"] # remove presence only choice
			# spThin_data_from_occ_dataset <- c(spThin_data_from_occ_dataset, "Your Biomod Presence/Absence dataset" = "biomod_pa_formated")
		# }
	# }

	if(!is.null(data_select())){
		spThin_data_from_occ_dataset <- c(spThin_data_from_occ_dataset, "Your subsetted Presence-Only dataset" = "selected_po_data")
		if(!is.null(input$User_Species_indicator_variable) && input$User_Species_indicator_variable %in% names(data_select())){
			if(all(c(0,1) %in% data_select()[,input$User_Species_indicator_variable])){
				spThin_data_from_occ_dataset = spThin_data_from_occ_dataset[spThin_data_from_occ_dataset!="selected_po_data"] # remove presence only choice
				spThin_data_from_occ_dataset <- c(spThin_data_from_occ_dataset, "Your subsetted Presence/Absence dataset" = "selected_pa_data")
			}
		}
	}

	if(!is.null(data_extraction())){
		spThin_data_from_occ_dataset <- c(spThin_data_from_occ_dataset, "Your extracted Presence-Only dataset" = "extracted_po_data")
		if('pa' %in% names(data_extraction())){
			spThin_data_from_occ_dataset = spThin_data_from_occ_dataset[spThin_data_from_occ_dataset!="extracted_po_data"]
			spThin_data_from_occ_dataset <- c(spThin_data_from_occ_dataset, "Your extracted occurence dataset" = "extracted_pa_data")
		}
	}

	if(!is.null(spThin_data_from_occ_dataset))
		updateSelectizeInput(session, inputId = "spThin_data_from_occ_dataset", choices = spThin_data_from_occ_dataset)
	else
		updateSelectizeInput(session, inputId = "spThin_data_from_occ_dataset", choices = NULL, options=list(placeholder="No occurence datasets available"))

})


# updates species choices
observe({

  input$spThin_data_from_occ_dataset
  data_select()

  isolate({

  	spthin_data_with_selected_species = NULL

  	# if(!is.null(input$spThin_data_from_occ_dataset) && input$spThin_data_from_occ_dataset=='biomod_po_formated'){
  		# Resp <- do.call('c', lapply(myBiomodFormatedObj(), function(x){ if(class(x) %in% c("BIOMOD.formated.data","BIOMOD.formated.data.PA")) {resp = all(x@data.species[!is.na(x@data.species)]==1); names(resp) <- x@sp.name; resp}else{NULL}} ) )
  		# spthin_data_with_selected_species <- names(Resp[Resp])
  	# }
  	# else if(!is.null(input$spThin_data_from_occ_dataset) && input$spThin_data_from_occ_dataset=='biomod_pa_formated'){
  		# Resp <- do.call('c', lapply(myBiomodFormatedObj(), function(x){ if(class(x) %in% c("BIOMOD.formated.data","BIOMOD.formated.data.PA")){resp = any(x@data.species[!is.na(x@data.species)]==0); names(resp) <- x@sp.name; resp}else{NULL}} ) )
  		# spthin_data_with_selected_species <- names(Resp[Resp])
  	# }
  	# else
  	if(!is.null(input$spThin_data_from_occ_dataset) && input$spThin_data_from_occ_dataset %in% c('extracted_po_data','extracted_pa_data')){
  		spthin_data_with_selected_species <- as.character(unique(data_extraction()[,input$User_Species_name]))
  	}
  	else if(!is.null(input$spThin_data_from_occ_dataset) && input$spThin_data_from_occ_dataset %in% c('selected_po_data','selected_pa_data')){
  		spthin_data_with_selected_species <- as.character(unique(data_select()[,input$User_Species_name]))
  	}

  	updateSelectizeInput(session, inputId = 'spThin_data_species', choices=spthin_data_with_selected_species)#, selected=spthin_data_with_selected_species[1])

  })

})

# updates species focal
observeEvent(input$spThin_data_species, ignoreNULL=TRUE, {
    speciesOcc$focal <- input$spThin_data_species
})


# updates occurrence points
observe({

	  isThinned()
    input$spthin_data_thinned
    input$spThin_hide_deleted_occ_data
    input$spthin_thin

    isolate({

			proxy <-leaflet::leafletProxy("SpThinMap")

			fn <- file.path(dirname(rasterTmpFile()), paste0(input$spThin_data_species,"_",input$spthin_NND,"_thin",if(is.null(input$spthin_data_thinned)) 1 else input$spthin_data_thinned,".csv"))

			# Add occurrence points
			if(isThinned() && file.exists(fn)){

			  dat_spthin = data_spthin()

				# identify points
				init_pts 				<- dat_spthin[,colnames(dat_spthin)%in%c('SPEC','LONG','LAT','RESP')]
				init_pts       <- unique(init_pts[,c('LONG','LAT')])
				thinned_pts 		<- read.csv(fn)

				id_thinned_pts 	<- which(duplicated(rbind(thinned_pts,init_pts)))-nrow(thinned_pts)
				init_pts$colPal	<- ifelse( (1:nrow(init_pts)) %in% id_thinned_pts, "retained", "removed")
				pal             <- if(length(unique(init_pts$colPal))==1L) leaflet::colorFactor("#4DAF4A",domain = "retained") else leaflet::colorFactor("Set1",domain=unique(init_pts$colPal))

				proxy %>% clearControls()

				proxy %>% clearShapes()

				proxy %>%

				  leaflet::addCircles(radius = 5000, data=init_pts, layerId=paste(1:nrow(init_pts)),

					lng = ~LONG,

					lat = ~LAT,

					color = if(length(unique(init_pts$colPal))==1L) "#4DAF4A" else ~pal(colPal),

					fillOpacity = 0.7
				)

				proxy %>%

				  leaflet::addLegend(position = "bottomleft", data=init_pts,

					title= "Records",

					pal = pal,

					values =  ~colPal,

					labels = unique(init_pts$colPal)
				)

				if(!is.null(input$spThin_hide_deleted_occ_data) && input$spThin_hide_deleted_occ_data){
				  proxy %>% removeShape(paste(setdiff(1:nrow(init_pts),id_thinned_pts)))
				}

				if(isThinned()){
				  can_save_thinned_data(TRUE)
				  can_delete_thinned_data(FALSE)
				}
			}
    })
	}
)


# --------------------------------------------------------

# UTILITY FUNCTIONS

# --------------------------------------------------------
# from stackoverflow...but can't find the source

LLtoUTMZone <- function(longitude, latitude) {

 # Special zones for Svalbard and Norway
    if (latitude >= 72.0 && latitude < 84.0 )
        if (longitude >= 0.0  && longitude <  9.0)
              return(31);
    if (longitude >= 9.0  && longitude < 21.0)
          return(33)
    if (longitude >= 21.0 && longitude < 33.0)
          return(35)
    if (longitude >= 33.0 && longitude < 42.0)
          return(37)

    (floor((longitude + 180) / 6) %% 60) + 1
}

LLToUTM <- function(lon,lat){
	ll <- data.frame(ID = 1:length(lon), LON = lon, LAT = lat)
	coordinates(ll) <- c("LON", "LAT")
	proj4string(ll) <- sp::CRS("+proj=longlat +datum=WGS84")
	zone = LLtoUTMZone(lon,lat)
	res <- spTransform(ll, sp::CRS(paste("+proj=utm +zone=",zone," +datum=WGS84",sep='')))
	return(res@coords)
}

# --------------------------------------------------------

# REACTIVE FUNCTIONS

# --------------------------------------------------------

leafMapSpThin <- shiny::reactive({

	# Draw map leaflet map
	map <- 	leaflet(options = leaflet::leafletOptions(worldCopyJump=T)) %>%

	  leaflet::addProviderTiles(leaflet::providers$Esri.OceanBasemap, group="Esri")

	# Add occurrence points
	if(!is.null(data_spthin())){

		map <- map %>%

		addCircles(radius = 5000, data=data_spthin(), layerId=paste(1:nrow(data_spthin())),

			lng = ~LONG,

			lat = ~LAT,

			color = "black",

			fillOpacity = 0.7
		)
	}

	# Set extent
	if( !is.null(data_spthin()) ){

		map <- map %>%

		setView(lat=median(data_spthin()[,'LAT']),lng=median(data_spthin()[,'LONG']), zoom=4, options=list(maxZoom=11))
	}
	else{
		map <- map %>%

		setView(lat=0,lng=0, zoom=3, options=list(maxZoom=11))
	}

	return(map)

})


# Returns an occurrence dataset for thinning process
data_spthin <- shiny::reactive({

	dat_spthin = NULL

		if(!is.null(input$spThin_data_from_occ_dataset) && length(input$spThin_data_from_occ_dataset)>0L && nchar(input$spThin_data_from_occ_dataset)>0L && !is.null(input$spThin_data_species) && nchar(input$spThin_data_species)>0L){

			if(input$spThin_data_from_occ_dataset %in% c('extracted_po_data','extracted_pa_data')){

				data_extrct <- data_extraction()
				dat_spthin = eval(parse(text=sprintf("data_extrct[data_extrct$%s=='%s',]",input$User_Species_name,input$spThin_data_species)))
				colnames(dat_spthin) <- replace(colnames(dat_spthin),colnames(dat_spthin)==input$User_XLongitude, 'LONG')
				colnames(dat_spthin) <- replace(colnames(dat_spthin),colnames(dat_spthin)==input$User_YLatitude,'LAT')
				colnames(dat_spthin) <- replace(colnames(dat_spthin),colnames(dat_spthin)==input$User_Species_name,'SPEC')
				colnames(dat_spthin) <- replace(colnames(dat_spthin),colnames(dat_spthin)=='pa', 'RESP')
			}
			else{
				# selected_pa_data
				data_slct 	<- data_select()
				dat_spthin = eval(parse(text=sprintf("data_slct[data_slct$%s=='%s',]",input$User_Species_name,input$spThin_data_species)))#eval(parse(text=sprintf("data_slct[data_slct$%s=='%s',]",input$User_Species_name,input$spThin_data_species)))
				colnames(dat_spthin) <- replace(colnames(dat_spthin),colnames(dat_spthin)==input$User_XLongitude, 'LONG')
				colnames(dat_spthin) <- replace(colnames(dat_spthin),colnames(dat_spthin)==input$User_YLatitude,'LAT')
				colnames(dat_spthin) <- replace(colnames(dat_spthin),colnames(dat_spthin)==input$User_Species_name,'SPEC')
				colnames(dat_spthin) <- replace(colnames(dat_spthin),colnames(dat_spthin)=='pa', 'RESP')
			}
		}

	return(dat_spthin)
})



# --------------------------------------------------------

# OUTPUTS

# --------------------------------------------------------


# displays user interface to specify settings for thinning process
output$spThin_analysis <- shiny::renderUI({

	out = tagList()

	if(!is.null(isolate(input$spThin_data_from_occ_dataset)) &&  isolate(input$spThin_data_from_occ_dataset) %in% c('biomod_pa_formated','extracted_pa_data'))
		out= tagList(out,radioButtons(inputId='labelled_PA_data',label=shiny::span(icon('question-circle',lib="font-awesome"),strong('Do you want to label your P/A data ?')),choices=c('YES', 'NO'), selected="NO", inline=TRUE))

	out= tagList(out,

		numericInput(inputId = 'spthin_NND',

			label = strong('Thinning distance (in km)'),

			value = 10,

			min = 0
		),
		numericInput(inputId = 'spthin_Nreps',

			label = strong('Number of repetitions'),

			value = 1,

			min = 1
		),

		checkboxInput(inputId='spThin_hide_deleted_occ_data', label='Hide deleted records',value=FALSE),

		br(),

		tags$style(appCSS),

		useShinyjs(),

		div(align='center',

			withBusyIndicatorUI(

				bsButton(inputId="spthin_thin", icon=icon('filter', lib="font-awesome"),

					label='Filter',

					style='primary'
				)
			)
		)

	)

	tagList(
		out,
		hr()
	)
})
outputOptions(output,"spThin_analysis", suspendWhenHidden=FALSE)


output$SpThinMap <- leaflet::renderLeaflet({

  return(leafMapSpThin())

})



# displays user interface to specify the type of diagnosis to displays (e.g. type of Ripley's functions, autocorrelation, ...etc)
# output$spThin_diagnosis <- shiny::renderUI({
#
# 	out = tagList()
#
# 		out= tagList(out, numericInput(inputId = 'spthin_data_thinned', label = 'Select your thinned dataset', value = NULL, min = 1), hr())
#
# 	out= tagList(out,
#
# 		h5("Diagnosis of convergence"),
#
# 		awesomeCheckbox(inputId='spthin_plot_convergence_test',label='Plot Number of max. records retained vs number of replications',value=TRUE, status = "primary"),
#
# 		checkboxInput(inputId='spthin_log_plot_convergence_test',label='log.scale',value=FALSE),
#
# 		h5("Point Patterns Process"),
#
# 		selectizeInput(inputId = "spthin_RipleyFunctions_extent_area",
#
# 			label = "Select area extent",
#
# 			choices = c('A bounding box area'='bounding_box','A polygon convex hull area'='convex_hull')
# 		),
#
# 		selectizeInput(inputId = "spthin_RipleyFunctions",
#
# 			label = "Ripley's K functions",
#
# 			choices = c('Univariate K function'='Kii', 'Linearized univariate K function'='Lii', 'Pair correlation G function'='g'),
#
# 			selected = 'Lii'
# 		)
# 	)
# 	if(!is.null(input$spThin_data_from_occ_dataset) && input$spThin_data_from_occ_dataset %in% c('biomod_po_formated','biomod_pa_formated','extracted_pa_data','extracted_po_data')){
#
# 		out= tagList(out,
#
# 			h5("Autocorrelation"),
#
# 			selectizeInput(inputId = 'spthin_LISA_variable',
#
# 				label = 'Select one variable',
#
# 				choices = NULL
# 			),
# 			selectizeInput(inputId = 'spthin_LISA_statistic',
#
# 				label = 'Select a statistic',
#
# 				choices = c("Local Moran's I"='I', "Local Geary's c"='c', "Local G"='G', "local G*"='G*', "Local K1"='K1')
# 			)
# 		)
# 	}
#
# 	tagList(
# 		h3("Diagnosis"),
# 		wellPanel(out)
# 	)
# })
# outputOptions(output,"spThin_diagnosis", suspendWhenHidden=FALSE)

