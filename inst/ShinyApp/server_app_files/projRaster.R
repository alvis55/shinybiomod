
# shows/hides different selectors in cascad
observe({
	if(!is.null(input$rast_proj_species_name) && nchar(input$rast_proj_species_name) > 0L)
		shinyjs::show("rast_proj_name_selector")
	else
		shinyjs::hide("rast_proj_name_selector")
})

observeEvent(input$rast_proj_name, ignoreNULL=TRUE, {
	if(nchar(input$rast_proj_name) > 0L)
		shinyjs::show("rast_model_proj_selector")
	else
		shinyjs::hide("rast_model_proj_selector")
})


# Updates input$rast_proj_species_name
observe({
	# Update select input for species name
		updateSelectizeInput(session, 'rast_proj_species_name', choices = listOfSpeciesToRastProj())
})

# Updates input$rast_proj_name
observe({
	# Update select input for projection name
		updateSelectizeInput(session, 'rast_proj_name', choices=listOfProjectsToRastProj())#selected=input$rast_proj_name)
})

# Updates input$rast_model_proj
observe({
  reordered = c(grep('mean',listOfModelsToRastProj()),grep('mean',listOfModelsToRastProj(),invert=TRUE))
	# Update select input for model projection name
	updateSelectizeInput(session, 'rast_model_proj', choices=listOfModelsToRastProj()[reordered])
})


# Updates the choices of the palette
observe({
  pals= NULL
  # Get available palette from palette type
  if(!is.null(input$palette_type))
    pals = row.names(subset(brewer.pal.info,category==input$palette_type))

  # Update select input for palette name
  updateSelectizeInput(session, 'palette_name', choices = pals)

})

# Updates input$ncolors
observe({
  # Get maximun number of colors from palette name
  maxColors = brewer.pal.info[input$palette_name,"maxcolors"]

  # Update select input for palette name
  updateNumericInput(session, 'ncolors', max=maxColors)
})

# Observer to update input$raster_gain
# observe({
# # Update select input for palette name
# updateNumericInput(session, 'raster_gain', value = input$raster_gain)
# })

# Updates input$colorRamp_checkbox
observeEvent(input$ncolors,{
  updateNumericInput(session, "ncolors", value = input$ncolors)
})

# Observer to update input$colorRamp_checkbox
observeEvent(input$colorRamp_checkbox,{
  updateCheckboxInput(session, "colorRamp_checkbox", value = input$colorRamp_checkbox)
})

# Updates the radio buttons of output raster format
observeEvent(input$raster_map_format, ignoreNULL=TRUE, {
  updateRadioButtons(session, 'raster_map_format', selected = input$raster_map_format)
})

# --------------------------------------------------------

# REACTIVE FUNCTIONS

# --------------------------------------------------------
my_Palette <- reactive({

  if(is.null(input$palette_name) || nchar(input$palette_name)<1)
    return(NULL)

  if(input$colorRamp_checkbox==TRUE)
    return( suppressWarnings(colorRampPalette(brewer.pal(n=input$ncolors, name=input$palette_name))(input$ncolors_ColorRamp)) )

  if(input$colorRamp_checkbox==FALSE)
    return( suppressWarnings(brewer.pal(n=input$ncolors, name=input$palette_name)) )

})


# Return list of species with projections
listOfSpeciesToRastProj <- reactive({

	if(!is.null(BiomodModeledObjDir()))
		list_spToRastProj <- list.files(BiomodModeledObjDir(), pattern = "^proj.+grd|asc$)",recursive=TRUE)
	else
		return(NULL)

	if(length(list_spToRastProj)<1 || nchar(list_spToRastProj)<1)
		return(NULL)

	list_species = unique(str_extract(list_spToRastProj,".+?(?=/)"))

	return(list_species)
})

# Return list of projections for a given species name
listOfProjectsToRastProj <- reactive({

	if(!is.null(input$rast_proj_species_name) && nchar(input$rast_proj_species_name)>0L){ #&& !is.null(BiomodModeledObjDir())

		file_dir = file.path(BiomodModeledObjDir(),input$rast_proj_species_name)

		if(length(file_dir)>0L && dir.exists(file_dir))
			list_projToRastProj <- grep("^proj",basename(list.dirs(file_dir,full.names=TRUE)),value=TRUE)#projs <- list.files(file_dir, pattern = sprintf("[{ensemble}|{%s}].grd$",input$rast_proj_species_name), recursive=TRUE)
		else
			return(NULL)

		if(length(list_projToRastProj)<1 || nchar(list_projToRastProj)<1)
			return(NULL)

		return(gsub("proj_",'',list_projToRastProj))
	}
	return(NULL)

})

# Return list of models of a given species and projection
listOfModelsToRastProj <- reactive({

	if(!is.null(input$rast_proj_name) && nchar(input$rast_proj_name)>0L){#!is.null(input$rast_proj_species_name) &&

		file_dir = paste(BiomodModeledObjDir(),input$rast_proj_species_name,paste0('proj_',input$rast_proj_name),sep="/")

		if(dir.exists(file_dir)){
			outfile <- grep(".out$",list.files(file_dir,full.names=TRUE),value=TRUE)

			if(length(outfile)<1)
				return(NULL)

			bm.out <- get(load(outfile))
		}
		else
			return(NULL)

			modelsToproj <- as.vector(sapply(get_projected_models(bm.out),gsub, pattern=sprintf("%s_",input$rast_proj_species_name), replacement="") )
			return(modelsToproj )
	}
	else
		return(NULL)

})

# Return raster model for a species and a given projection
rast_model_proj <- reactive({

	req(input$rast_proj_species_name,input$rast_model_proj)

	if(input$rast_model_proj %in% listOfModelsToRastProj()){

		file_dir = paste(BiomodModeledObjDir(),input$rast_proj_species_name,paste0('proj_',input$rast_proj_name),sep="/")

		if(dir.exists(file_dir)){
			outfile <- grep(".out$",list.files(file_dir,full.names=TRUE),value=TRUE)
			if(length(outfile)<1)
				return(NULL)
			bm.out <- get(load(outfile))
		}
		else
			return(NULL)

		list.rasters <- raster::stack(file.path(BiomodModeledObjDir(),bm.out@proj@link))

		mod_proj <- subset(list.rasters, subset=paste(input$rast_proj_species_name,input$rast_model_proj,sep='_'))
		#gain(mod_proj) <- input$raster_gain

		if(!hasValues(mod_proj))
			return(NULL)

		return(mod_proj)
	}
	else
		return(NULL)

})

# --------------------------------------------------------

# OUTPUTS

# --------------------------------------------------------
output$myPaletteType <- renderPrint({

	palette_type = if(!is.null(input$palette_type)) input$palette_type else 'seq'

	switch(palette_type,

		'seq' = helpText("Note: sequential palettes are suited to ordered data",
					"that progress from low to hight.",
					"Light colors for low data, dark for high data."),

		'div' = helpText("Note: diverging palettes put equal emphasis on mid-range",
				"critical values and extremes at both ends of the data range.",
				"Light colors for mid-range data, low and high contrasting dark colors."),

		'qual' = helpText("Note: qualitative palettes do not imply magnitude differences",
				 "between legend classes, and hues are used to create the",
				"primary visual differences between classes.")
	)
})
outputOptions(output,'myPaletteType',suspendWhenHidden=FALSE)

output$probMap <- renderPlot({

	if(!is.null(rast_model_proj())){

		r <- rast_model_proj()
		gain(r) <- input$raster_gain

		myTheme = my_Palette()

		# Plot
		levelplot(r,
			par.settings = rasterTheme(region = myTheme),
			margin = FALSE,
			height=900,
			width=900#,colorkey = list(space = "bottom")
		)
	}
	else{
		messages <- "Load your model"
		x <- -10:10
		y <- x
		plot(x,y,type="n", xlab="No Data", ylab="No data",cex=2)
		text(0,0,messages,cex=4,col="grey")
	}

},height=600)
#,width=900

#https://github.com/rstudio/shiny/issues/680
output$download_proba_raster_map <- downloadHandler(

  filename = function(){
    paste0(input$rast_proj_species_name,"_",input$rast_model_proj,switch(input$raster_map_format,"ASC"=".asc","TIFF"=".tif"))
  },

  content = function(file) {
    r <- rast_model_proj()
    if(!is.null(r)){
      ext <- switch(input$raster_map_format,"ASC"=".asc","TIFF"=".tif")
      output_filename <- paste0(raster::rasterTmpFile(),ext)
      raster::gain(r) <- input$raster_gain
      out <- try(raster::writeRaster(r, output_filename, format=switch(input$raster_map_format,"ASC"="ascii","TIFF"="GTiff"), overwrite=TRUE),silent=TRUE)

      if(file.exists(output_filename)){
        # Rename it to the correct filename
        file.rename(out@file@name, file)
      }
    }

  },
  contentType ='image/tiff'
)


