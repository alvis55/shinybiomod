# --------------------------------------------------------

# GLOBAL geographic features

# --------------------------------------------------------
shbmd_geodata <- reactiveValues(
  terrestrial_lands = lwgeom::st_transform_proj(RGeodata::terrestrial_lands, crs="+proj=wintri"),
  ecoregions = lwgeom::st_transform_proj(RGeodata::ecoregions, crs="+proj=wintri")
)

# --------------------------------------------------------

# GLOBAL species parameters

# --------------------------------------------------------
speciesOcc <- reactiveValues(focal=NULL)


# --------------------------------------------------------

# GLOBAL environmental parameters

# --------------------------------------------------------
global_env_variables <- c('SHINY_BIOMOD_OUTPUT_DIR',
                          'SHINY_BIOMOD_RASTER_DIR',
                          'SHINY_BIOMOD_REGION_DIR',
                          'SHINY_BIOMOD_BACKGROUND_DIR',
                          'SHINY_BIOMOD_ARCGIS_DIR')
# find Rprofile & Renviron files
# https://stackoverflow.com/questions/13735745/locate-the-rprofile-file-generating-default-options
path2Renviron <- function() {
  candidates <- c( file.path(Sys.getenv("R_USER"),".Renviron"),
                   file.path(Sys.getenv("USERPROFILE"),".Renviron"),
                   file.path(R.home(),"etc",".Renviron"),
                   file.path(getwd(), ".Renviron") )
  tmp <- Filter(file.exists, candidates)

  # get global Renviron only
  grep("ShinyBIOMOD", tmp, invert=TRUE, value=TRUE)
}

# copy Renviron file (if needed) to the app home directory and edit current path
if(!identical(path2Renviron(), character(0)) && nzchar(path2Renviron()) && file.exists(path2Renviron()) && !file.exists(file.path(appdir(),".Renviron"))){
  file.copied <- file.copy(from=path2Renviron(), to=file.path(appdir(),".Renviron"))
  if(!file.copied){
    warning("Unable to copy .Renviron file: ",path2Renviron())
  }else{
text.to.write <-sprintf("
# setting the path to Rprofile and Renviron
R_PROFILE = '%s'
R_ENVIRON_USER = '%s'
", file.path(appdir(),".Rprofile"), file.path(appdir(),".Renviron"))

  if(Sys.getenv('R_PROFILE')!=file.path(appdir(),".Rprofile") ||
     Sys.getenv('R_ENVIRON_USER') != file.path(appdir(),".Renviron")){
    success <- try(cat(text.to.write, file=file.path(appdir(),".Renviron"), sep='\n', append=TRUE), silent=TRUE)
    if(inherits(success,"try-error"))
      warning("Unable to update .Renviron")
  }
    Sys.setenv(R_ENVIRON_USER=file.path(appdir(),".Renviron"))
    readRenviron(Sys.getenv("R_ENVIRON_USER"))
  }
}else{
  text.to.write <-sprintf("
# setting the path to Rprofile and Renviron
R_PROFILE = '%s'
R_ENVIRON_USER = '%s'
", file.path(appdir(),".Rprofile"), file.path(appdir(),".Renviron"))

  if(Sys.getenv('R_PROFILE')!=file.path(appdir(),".Rprofile") ||
     Sys.getenv('R_ENVIRON_USER') != file.path(appdir(),".Renviron")){
    success <- try(cat(text.to.write, file=file.path(appdir(),".Renviron"), sep='\n', append=TRUE), silent=TRUE)
    if(inherits(success,"try-error"))
      warning("Unable to update .Renviron")
  }
  Sys.setenv(R_ENVIRON_USER=file.path(appdir(),".Renviron"))
  readRenviron(Sys.getenv("R_ENVIRON_USER"))
}

path2Rprofile <- function() {

  candidates <- c( Sys.getenv("R_PROFILE"),
                   file.path(Sys.getenv("R_HOME"), "etc", "Rprofile.site"),
                   Sys.getenv("R_PROFILE_USER"),
                   file.path(Sys.getenv("R_HOME"),".Rprofile"),
                   file.path(getwd(), ".Rprofile") )
  tmp <- Filter(file.exists, candidates)

  # get global Rprofile only
  grep("ShinyBIOMOD", tmp, invert=TRUE, value=TRUE)
}

# copy Rprofile file (if needed) to the app home directory
if(!identical(path2Rprofile(), character(0)) && nzchar(path2Rprofile()) && file.exists(path2Rprofile()) && !file.exists(file.path(appdir(),".Rprofile"))){
  file.copied <- file.copy(from=path2Rprofile(), to=file.path(appdir(),".Rprofile"))
  if(!file.copied){
    warning("Unable to copy .Rprofile file: ",path2Rprofile())
  }else{

text.to.write <-"
# setting CRAN mirror
local({
  r <- getOption('repos')
  r['CRAN'] <- 'https://cloud.r-project.org'
  options(repos=r)
  rm(r)
})"
    if(!nzchar(getOption('repos'))){
      success <- try(cat(text.to.write, file=file.path(appdir(),".Rprofile"), sep='\n', append=TRUE), silent=TRUE)
      if(inherits(success,"try-error"))
        warning("Unable to update .Rprofile")
    }
    # read profile
    source(file.path(appdir(),".Rprofile"))
  }
}else{

  text.to.write <-"
# setting CRAN mirror
local({
  r <- getOption('repos')
  r['CRAN'] <- 'https://cloud.r-project.org'
  options(repos=r)
  rm(r)
})"
  if(!nzchar(getOption('repos'))){
    success <- try(cat(text.to.write, file=file.path(appdir(),".Rprofile"), sep='\n', append=TRUE), silent=TRUE)
    if(inherits(success,"try-error"))
      warning("Unable to update .Rprofile")
  }
  # read profile
  source(file.path(appdir(),".Rprofile"))
}


rasters 	          <- reactiveValues(origins=NULL, area=NULL, background=NULL)
rasters_dir   <- ""
workflow_output_dir <- ""


