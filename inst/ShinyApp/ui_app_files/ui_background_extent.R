background_extent <- fluidPage(

			useShinyjs(),

			extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),

			titlePanel(h1(strong(em("Geographic constraint of the training area")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

				sidebarLayout(position = "left",

					sidebarPanel(

					  hr(),

						p("Define the geographic area in which to train your model."),
						tags$ul(style="font-family: 'times';font-size: 18px;",
						  tags$li("Only the occurrence records falling inside this area will be used for training your model."),
              tags$li(em(strong("Pseudo-Absences"),'or background points'),"will be generated from this area.")
						),

						hr(),

						#"Draw a constrained area from the map (Not implemented yet)" = 'TOOL',
						radioButtons(inputId='background_extent',label=h4(strong("Choose a method:")),choices=c("Use a pre-defined region" = 'POLY'), selected="TOOL"),

						div(class="info",id="background_extent_tooltip_info_occ_data",style="border: 2px solid #e7f3fe; text-align: justify;",
						    p(style="font-size: 16px;",shiny::span(icon('lightbulb', lib="font-awesome"),
						                                           strong("Tip:"),"Upload your occurrence data in",em("Data Upload -> Occurrence Data"),"to see options using occurrence locations"))
						),

						radioButtons(inputId='clip_background_extent',label=h4(strong("Clip your area:")),choices=c("To coastline" = 'COAST', "Off the lands" = 'LAND', "Do not clip" = 'NO'), selected="COAST", inline=TRUE),


						hidden(

							div(id="background_extent_occ_data_panel",

								wellPanel(style='background-color: lavender',

									h3("Occurrence data"),

									p(strong("Select your occurrence dataset")),

									selectizeInput(inputId='occ_data_for_background_extent', label=NULL, choices=NULL),

									uiOutput("chooserConstrainedAreaSpecies"),

									div(id="background_extent_buffer_panel",

				              h3("Buffer parameter"),

				              p(strong('Select the size of your buffer')),

				              numericInput(inputId='background_extent_buffer_size', label=NULL, value=100, min=0),

				              helpText(em('Note: Give the radius size in kilometers'))
									)
								)
							)
						),

						hidden(

							div(id="background_extent_polygon_panel",

								wellPanel(style='background-color: lavender',

									h3("background data"),

									p(strong("Select the directory of your background shapefile")),

									directoryInput(inputId='backgroundshape_directory', label = NULL),

									tags$style(appCSS),

									selectizeInput(inputId="backgroundShape_files",

										label=NULL,

										choices = NULL,

										options=list(placeholder = "Select a background layer")
									),

									awesomeCheckbox(inputId="checkBackgroundMap", label="Show background map", value = FALSE, status = "primary")

								),
								uiOutput("BackgroundFeatures")
							)

						),

						hidden(
							div(id='background_extent_add_occ_data_div',
								checkboxInput(inputId='background_extent_add_occ_data', label='Add occurrence data',value=TRUE)
							)
						),

						br(),

						div(align='center', withBusyIndicatorUI(bsButton(inputId="background_extent_Clip", icon=icon('crop-alt', lib='font-awesome'), label='Clip', style="primary"))),

						hr()

					),

					mainPanel(

							tabPanel("Map",

							  withSpinner(leafletOutput(outputId="BgMap", width = "100%", height="700px"),type=5,size=1, color='#000000'),

								hidden(
								  div(id="Background_extent_Polygon_Info",

  									tags$style(appCSS),

  									useShinyjs(),

  									uiOutput("polygonInfo")
								  )
								)
							)
					)
				)
)
